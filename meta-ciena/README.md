5 April 2021

Pulling from Upstream

To allow for pulling from upstream is pretty simple:

$ git checkout master
$ git remote add upstream https://github.com/openbmc/openbmc.git

$ git remote -v
origin    ssh://git@bitbucket.ciena.com/cediag/openbmc.git (fetch)
origin    ssh://git@bitbucket.ciena.com/cediag/openbmc.git (push)
upstream    https://github.com/openbmc/openbmc.git (fetch)
upstream    https://github.com/openbmc/openbmc.git (push)

$ git pull upstream --rebase

Resolve any conflicts. 
I would not expect there to be any in master branch.
Build: This should be tested against the AST-eval board as there are no 
Ciena changes. 

Push changes into bitbucket
$ git push origin master

Now merged changes into ciena branch
$ git checkout ciena
$ git rebase master

Resolve any conflicts.
Build and test on 8112 and 8199


