This layer allows you to build OpenBMC for 8112
without having to manually configure bblayers.conf and local.conf.

The AST2600 is an ARM, service management SOC made by ASPEED. More information
about the AST2600 can be found
[here](http://aspeedtech.com/server_ast2600/).

To Build SPI flash image: (MACHINE = 8112spi)

  $ cd {openbmc}
  $ . setup 8112spi
  $ bitbake obmc-phosphor-image

  {build} = {openbmc}/build/8112spi

  Images in {build}/tmp/deploy/images/8112spi:
    - image-bmc - SPI flash image

To Build eMMC image: (MACHINE = 8112emmc)

  $ cd {openbmc}
  $ . setup 8112emmc
  $ bitbake obmc-phosphor-image

  {build} = {openbmc}/build/8112emmc

  Images in direcotry {build}/tmp/deploy/images/8112emmc:
    - obmc-phosphor-image-8112emmc.wic    - eMMC image
    - obmc-phosphor-image-8112emmc.wic.gz - eMMC compressed image

