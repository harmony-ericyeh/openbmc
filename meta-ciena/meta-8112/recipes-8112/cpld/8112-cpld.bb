PR = "r1"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10 \
                   "
FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"
SRC_URI_append = " \
    file://ast-jtag.c \
    file://main.c \
    file://svf.c \
    file://ast-jtag.h \
    file://svf.h \
    file://Makefile \
    file://cpld_version.sh \
                 "
S = "${WORKDIR}"
TARGET_CC_ARCH += "${LDFLAGS}"
RDEPENDS_${PN} += "bash"

do_compile() {
    oe_runmake -C ${B}
}

do_install() {
    install -d ${D}${sbindir}
    install -m 0755 ${B}/svf ${D}${sbindir}/
    install -m 0755 ${B}/cpld_version.sh ${D}${sbindir}/
}

FILES_${PN} = "${sbindir}/*"
