#!/bin/sh
#
# Check version of cpld. Here register 0x1E780700 gives bit 0-31 and
# 0x1E780704 represents bit 32 onwards. Analyze the bit 47-40, it should
# represent the version of CPLD (05 for below example).
#
# Example:
#
#   # devmem 0x1E780700
#   0xFF807F7E
#   # devmem 0x1E780704
#   0x0000057F
#         ^^
GPIO704="0x1E780704"
ver=`devmem $GPIO704 | cut -c7-8`
echo $ver

exit 0
