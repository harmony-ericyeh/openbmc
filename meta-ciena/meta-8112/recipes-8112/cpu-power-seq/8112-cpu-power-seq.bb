PR = "r1"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10 \
                   "
inherit systemd
#inherit obmc-phosphor-systemd
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "8112-cpu-power-seq.service 8112-gpio-config.service"

DEPENDS += "libgpiod"

RDEPENDS_${PN} += " \
    bash \
    libgpiod \
    8112-obmc-installer \
"

S = "${WORKDIR}"

SRC_URI += " file://cpu_power_seq.c \
             file://8112-cpu-power-seq.service \
             file://gpio_config.c \
             file://8112-gpio-config.service \
           "

do_compile() {
    ${CC} ${S}/cpu_power_seq.c -o cpu_power_seq ${CFLAGS} ${LDFLAGS} -lgpiod -lpthread
    ${CC} ${S}/gpio_config.c -o gpio_config ${CFLAGS} ${LDFLAGS} -lgpiod
}

do_install() {
    install -d ${D}${sbindir}
    install -m 0755 ${B}/cpu_power_seq ${D}${sbindir}/
    install -m 0755 ${B}/gpio_config ${D}${sbindir}/

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${S}/8112-cpu-power-seq.service ${D}${systemd_system_unitdir}
    install -m 0644 ${S}/8112-gpio-config.service ${D}${systemd_system_unitdir}
}

