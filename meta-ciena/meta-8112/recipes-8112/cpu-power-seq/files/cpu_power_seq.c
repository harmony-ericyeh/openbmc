/* Copyright (c) 2021 Ciena Corporation, All Rights Reserved */
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <gpiod.h>
#include <signal.h>
#include <string.h>

typedef enum
{
    CPU_POWER_OFF,
    CPU_POWER_ON,
    CPU_POWER_CYCLE 
} cpu_power_action;

typedef enum
{
    CONSOLE_BMC,
    CONSOLE_X86
} fp_console;

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

struct gpio_list {
    unsigned int gpio_num;
    char *gpio_name;
};

#define GPIO_BIOS_SWITCH    150
#define CPLD_CPU_ERROR_2    (1<<10)
#define CPLD_CPU_CATERR_N   (1<<11)
#define PCH_SYSRESET_N      (1<<(38-31))
#define CPLD_SC_PWRGOOD     (1<<23)

//#define GPIOV7 175
#define GPIOA4 4
#define GPIOR1 137
#define GPIO_BMC_STATUS0 40
#define GPIO_BMC_STATUS1 41

#define SCU414  0x1e6e2414  // Multi-function Pin Control #5
#define GPIO700 0x1e780700  // SGPIO Slave 1 initiator Data Register #1
#define GPIO704 0x1e780704  // SGPIO Slave 1 initiator Data Register #2

#define GPIO_BULK_NUM 18
struct gpio_list p_gpio_list[GPIO_BULK_NUM] = {
    { 45, "GPIOF5"},
    {113, "GPIOO1"},
    {132, "GPIOQ4"},
    { 46, "GPIOF6"},
    {129, "GPIOQ1"},
    {170, "GPIOV2"},
    {130, "GPIOQ2"},
    {136, "GPIOR0"},
    { 43, "GPIOF3"},
    {175, "GPIOV7"},
    {137, "GPIOR1"},
    {135, "GPIOQ7"},
    { 70, "GPIOI6"},
    {172, "GPIOV4"},
    {128, "GPIOQ0"},
    { 69, "GPIOI5"},
    {134, "GPIOQ6"},
    {168, "GPIOV0"}
};

/* log */
#define FFLUSH  { fflush(stdout); fflush(stderr); }
#define LOG_SIZE 256    
char log_buf[LOG_SIZE];
#define LOG(...) {                               \
    snprintf(log_buf, LOG_SIZE, ## __VA_ARGS__); \
    printf("%s\n", log_buf);                     \
    FFLUSH;                                      \
}

/* power commands */
#define POWER_ON  \
    "gpioset -m time -u 500000 0 71=0; gpioset -m time -s 1 0 71=1"
#define POWER_OFF \
    "gpioset -m time -s 5 0 71=0; gpioset -m time -s 5 0 71=1"
#define POWER_CYCLE \
    "gpioset -m time -s 5 0 71=0;gpioset -m time -s 5 0 71=1; \
    gpioset -m time -u 500000 0 71=0; gpioset -m time -s 1 0 71=1"

/* global definition */
struct gpiod_line_bulk bulk = GPIOD_LINE_BULK_INITIALIZER;
struct gpiod_chip *chip;
struct gpiod_line *line[GPIO_BULK_NUM], *gpio_line_gpio_a4, *gpio_line_gpio_r1;
struct gpiod_line *gpio_bmc_status0, *gpio_bmc_status1, *gpio_line_bios_switch;
unsigned int gpio_number = 0;
unsigned int boot_count = 0;
unsigned int gpio_values[GPIO_BULK_NUM] = { 0 };
unsigned int gpio_reverse_index = GPIO_BULK_NUM;
unsigned int active_bios = 0;
unsigned int bmc_reboot = 0;

/* function prototype */
void cpu_power_change(cpu_power_action action);
int  cpu_power_on(void);
int  cpu_not_ready(void);
int  gpio_init(void);
int  gpio_monitor_list(void);
int  gpio_r1_monitor(void);
void int_handler(int);
int  pre_power(void);
int  post_power(void);
int  power_on_wait(int wait_ms);
int  read_devmem(off_t target, unsigned long *read_value, unsigned long writeval);
int  start_timer(void);
void switch_console(fp_console conf);

//#define DEBUG

#ifdef DEBUG
#define DEBUG_TIMEOUT   300
void debug_handler(int signum);
void debug_set(int val);
#endif

/* use local timer as watchdog timer */
#ifndef DEBUG
#define TIMEOUT 22
#define CPU_ON  "/tmp/cpu-on"
#else
#define TIMEOUT 60
#define CPU_ON  "/tmp/debug"
#endif

/*
 * main
 */
int main(int argc, char *argv[])
{
    struct timespec ts;
    int i, rval;
    struct sigaction sa_int;

#ifdef DEBUG
    /* intall debug timer switch to BMC console */
    struct sigaction sa_alrm;

    LOG("Add debug timer handler");
    sa_alrm.sa_handler = debug_handler;
    sigaction(SIGALRM, &sa_alrm, NULL);
    debug_set(DEBUG_TIMEOUT);
#endif

    /* install interrupt handler */
    LOG("Add interrupt  handler");
    sa_int.sa_handler = int_handler;
    sigaction(SIGINT, &sa_int, NULL);

    /* init gpios */
    rval = gpio_init();
    if (rval != 0)
        return (rval);

    /* pre check for bmc reboot case */
    rval = pre_power();
    if (rval != 0)
        return (rval);

    /* start cpu power monitoring */
    while (1)
    {
        /* list gpio monitoring list */
        rval = gpio_monitor_list();
     	if (rval != 0)
            return (rval);
             
        /* cpu power on */
        rval = cpu_power_on();
     	if (rval != 0)
            return (rval);
        LOG("Starting CPU...driving GPIOA4 high!!");

        /* wait for 5 seconds */
        rval = power_on_wait(5000);
        if (rval != 0)
            return (rval);

        gpiod_line_release_bulk(&bulk);

        /* received golden word */
        if ((gpio_number & 0x1FF) == 0x1FF) {
#ifndef DEBUG
            switch_console(CONSOLE_X86);
#endif
            LOG("boot_count=%d, bmc_reboot=%d", boot_count, bmc_reboot);

            if (bmc_reboot) {
                rval = gpio_r1_monitor();
                if (rval != 0)
                    return (rval);
            } else {
                /* check cpu successful boot */
                rval = start_timer();
                if (rval == 0) {
                    post_power();
                    rval = gpio_r1_monitor();
                    if (rval != 0)
                        return (rval);
                } else {
                    /* timeout occurred, cpu not ready */
                    switch_console(CONSOLE_BMC);
                    cpu_not_ready();
                    cpu_power_change(CPU_POWER_CYCLE);
                }
            }
     	}
        /* not received golden word */
     	else
     	{
            /* slp_s3 and slp_s4 on normal condition */
            if ((gpio_number & 1) && (gpio_number & 2))
                cpu_not_ready();
            LOG("boot_count=%d, bmc_reboot=%d", boot_count, bmc_reboot);
            cpu_power_change(CPU_POWER_CYCLE);
     	}

        gpiod_line_release(gpio_line_gpio_a4);
        gpio_line_gpio_a4 = NULL;
    }
    return 0;
}

/* switch console */
void switch_console(fp_console conf)
{
    int rval;

    FFLUSH
    switch (conf) {
        case CONSOLE_BMC:
            LOG("Switching BMC Console");
            rval = system("i2cset -y 5 0x20 0xC 0x02");
            break;
        case CONSOLE_X86:
            LOG("Switching x86 Console");
            sleep(2);
            rval = system("i2cset -y 5 0x20 0xC 0x01");
            break;
    }
}

/* calculate time difference */
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

/* change cpu power */
void cpu_power_change(cpu_power_action action)
{
    int rval;
    LOG("cpu_power_change()...Enter:action=%d", action);
    switch (action) {
        case CPU_POWER_OFF:
            rval = system(POWER_OFF);
            break;
        case CPU_POWER_ON:
            rval = system(POWER_ON);
            break;
        case CPU_POWER_CYCLE:
            rval = system(POWER_CYCLE);
            break;
    }
    LOG("cpu_power_change()...Exit");
}

/* initialize gpio setting */
int gpio_init(void)
{
    int i, rval;

    gpio_line_gpio_a4 = NULL;
    gpio_line_bios_switch = NULL;

    chip = gpiod_chip_open("/dev/gpiochip0");
    if (!chip) {
       LOG("failure: gpiod_chip_open()");
       return (-1);
    }

    for (i = 0; i < GPIO_BULK_NUM; i++) {
        line[i] = gpiod_chip_get_line(chip, p_gpio_list[i].gpio_num);
        if (!line[i]) {
            LOG("Error: gpiod_chip_get_line failed for %d",
                p_gpio_list[i].gpio_num);
            gpiod_chip_close(chip);
            return (-1);
        }
        gpiod_line_bulk_add(&bulk, line[i]);
    }

    /* GPIOF0 */
    gpio_bmc_status0 = gpiod_chip_get_line(chip, GPIO_BMC_STATUS0);
    if (!gpio_bmc_status0) {
        LOG("Error: gpiod_chip_get_line failed for GPIO_BMC_STATUS0");
        gpiod_chip_close(chip);
        return (-1);
    }
    rval = gpiod_line_request_output(gpio_bmc_status0,
        "cpu_power_seq", GPIOD_LINE_ACTIVE_STATE_HIGH);
    if (rval != 0) {
        LOG("Error: gpiod_line_request_output() failed for GPIO_BMC_STATUS0");
        gpiod_chip_close(chip);
        return (-1);
    }

    /* GPIOF1 */
    gpio_bmc_status1 = gpiod_chip_get_line(chip, GPIO_BMC_STATUS1);
    if (!gpio_bmc_status1) {
        LOG("Error: gpiod_chip_get_line failed for GPIO_BMC_STATUS1");
        gpiod_chip_close(chip);
        return (-1);
    }
    rval = gpiod_line_request_output(gpio_bmc_status1,
        "cpu_power_seq", GPIOD_LINE_ACTIVE_STATE_HIGH);
    if (rval != 0) {
        LOG("Error: gpiod_line_request_output() failed for GPIO_BMC_STATUS1");
        gpiod_chip_close(chip);
        return (-1);
    }

    LOG("Starting CPU...driving GPIOF0 and GPIOF1 low");
    gpiod_line_set_value(gpio_bmc_status0, 0);
    gpiod_line_set_value(gpio_bmc_status1, 0);

    return (0);
}

/* wait for receive golden word in 5 seconds */
int power_on_wait(int wait_ms)
{
    struct timeval t0, t1;
    unsigned long elapsed;
    unsigned long reg700, reg704;
    int i, rval;

 	gettimeofday(&t0, 0);
  	gpio_number = 0;
 	while ((gpio_number & 0x1FF ) != 0x1FF)
 	{
    	gpio_number = 0;
        gettimeofday(&t1, 0);
        elapsed = timedifference_msec(t0, t1);
        rval = gpiod_line_get_value_bulk(&bulk, gpio_values);
        if (rval != 0) {
            LOG("Error: gpiod_line_request_bulk_input() failed");
            gpiod_line_release_bulk(&bulk);
            gpiod_chip_close(chip);
            return (-1);
        }

        for (i = 0; i < GPIO_BULK_NUM; i++) {
            gpio_number = (gpio_number << 1) | gpio_values[i];
        }

        gpio_number = gpio_number |
            (gpiod_line_get_value(gpio_line_gpio_a4) << i);
        rval = read_devmem(GPIO700, &reg700, 0);
        if (rval != 0) {
            LOG("Error: read_devmem() read for GPIO700");
            gpiod_chip_close(chip);
            return (-1);
        }
        rval = read_devmem(GPIO704, &reg704, 0);
        if (rval != 0) {
            LOG("Error: read_devmem() read for GPIO704");
            gpiod_chip_close(chip);
            return (-1);
        }
        LOG("%ld ms : GPIO[%d-0]=%x GPIO700=0x%08lx GPIO704=0x%08lx",
            elapsed, GPIO_BULK_NUM, gpio_number, reg700, reg704);

        // run 5 seconds during cpu power on,
        // precheck 1 second when bmc reboot
        if (elapsed > wait_ms) {
            break;
        }
        usleep(100000);
    }
    return (0);
}

/* monitor GPIOR1 in loop after cpu successful boot */
int gpio_r1_monitor(void)
{
    struct gpiod_line_event ev;
    struct timespec ts;
    int rval;

    gpio_line_gpio_r1 = gpiod_chip_get_line(chip, GPIOR1);
    if (!gpio_line_gpio_r1) {
        LOG("Error: gpiod_chip_get_line failed for %d", GPIOR1);
        gpiod_chip_close(chip);
        return (-1);
    }

    rval = gpiod_line_request_both_edges_events(gpio_line_gpio_r1, "cpu_power_seq");
    if (rval != 0) {
        LOG("Error: gpiod_line_request_both_edges_events()"
            "failed for GPIOR1!!");
        gpiod_chip_close(chip);
        return (-1);
    }
    LOG("Enabled GPIO Event monitoring for GPIOR1");

    while (1) // stop this process from exiting...!!!
    {
        ts.tv_sec = 0x7FFFFFFF;
        ts.tv_nsec = 0;
        rval = gpiod_line_event_wait(gpio_line_gpio_r1, &ts);
        if (rval < 0) {
            LOG("Wait event notification failed");
        } else if (rval == 0) {
            LOG("Wait event notification on timeout");
            continue;
        }
        rval = gpiod_line_event_read(gpio_line_gpio_r1, &ev);
        LOG("Get event notification on GPIOR1");
        if (rval < 0) {
            LOG("Read last event notification failed");
        }
        if (ev.event_type == GPIOD_LINE_EVENT_RISING_EDGE) {
            LOG("GPIOD_LINE_EVENT_RISING_EDGE on GPIOR1");
        } else {
            LOG("GPIOD_LINE_EVENT_FALLING_EDGE on GPIOR1");
        }
    }

    /* not reach here */
    return (0);
}

/* 
 * Local timer to monitor cpu successful boot.
 * - OpenBMC kcs bridge creates CPU_ON as indication.
 * - Poll IPMI command from BIOS:
 *    0x06:0x24 - Set Watchdog
 *    0x06:0x25 - Get Watchdog
 */
int start_timer(void)
{
    struct stat sb;
    int i = 0;

    printf("Start Timer - %d seconds\n", TIMEOUT);
    while (stat(CPU_ON, &sb) != 0) {
#ifdef DEBUG
        LOG("Timer - %d seconds", (TIMEOUT - i));
#endif
        sleep(1);
        if (i++ >= TIMEOUT) {
            LOG("Timer Expired");
            return (-1);
        }
    }
    LOG("CPU Successful Boot");
    return (0);
}

/* display monitoring gpio list */
int gpio_monitor_list(void)
{
    int i, rval;

 	rval = gpiod_line_request_bulk_input(&bulk, "cpu_power_seq");
 	if (rval != 0) {
        LOG("Error: gpiod_line_request_bulk_input() failed ");
        gpiod_chip_close(chip);
        return (-1);
 	}
 	LOG("GPIO Monitoring List:=> ");
 	LOG("bit position=%03d,gpio number=%03d,gpio name=%s",
            GPIO_BULK_NUM, 4, "GPIOA4");
 	for (i = 0, gpio_reverse_index = GPIO_BULK_NUM;
        i < GPIO_BULK_NUM; i++, gpio_reverse_index--) {
        LOG("bit position=%03d,gpio number=%03d,gpio name=%s",
            (gpio_reverse_index - 1), p_gpio_list[i].gpio_num,
            p_gpio_list[i].gpio_name);
 	}
    return (0);
}

/* enable cpu power */
int cpu_power_on(void)
{
    unsigned long reg_value;
    int rval;

    /* GPIOA4 */
    gpio_line_gpio_a4 = gpiod_chip_get_line(chip, GPIOA4);
    if (!gpio_line_gpio_a4) {
        LOG("Error: gpiod_chip_get_line failed for %d", GPIOA4);
        gpiod_chip_close(chip);
        return (-1);
    }

    rval = gpiod_line_request_output(gpio_line_gpio_a4,
        "cpu_power_seq", GPIOD_LINE_ACTIVE_STATE_HIGH);
    if (rval != 0) {
        LOG("Error: gpiod_line_request_output() failed for GPIOA4");
        gpiod_chip_close(chip);
        return (-1);
    }

    rval = read_devmem(SCU414, &reg_value, 0);
    if (rval != 0) {
        LOG("Error: read_devmem() reading for SCU414");
        gpiod_chip_close(chip);
        return (-1);
    }
    LOG("read_devmem():read  : SCU414=0x%08lx", reg_value);

    rval = read_devmem(SCU414, NULL, reg_value | 0xF0000000);
    if (rval != 0) {
        LOG("Error: read_devmem() writing for SCU414");
        gpiod_chip_close(chip);
        return (-1);
    }
    LOG("read_devmem():wrote : SCU414=0x%08lx", reg_value);

    rval = read_devmem(SCU414, &reg_value, 0);
    if (rval != 0) {
        LOG("Error: read_devmem() reading for SCU414");
        gpiod_chip_close(chip);
        return (-1);
    }
    LOG("read_devmem():read  : SCU414=0x%08lx", reg_value);

    /* GPIOA4 */
    gpio_line_gpio_a4 = gpiod_chip_get_line(chip, GPIOA4);
    if (!gpio_line_gpio_a4) {
        LOG( "Error: gpiod_chip_get_line failed for %d", GPIOA4);
        gpiod_chip_close(chip);
        return (-1);
    }
    gpiod_line_set_value(gpio_line_gpio_a4, 1);
    return (0);
}

/* 
 * process cpu not ready due to timer expired
 * - when boot_count is 1, power-cycle cpu
 * - when boot_count is 2, switch to golden bios then power-cycle cpu
 */
int cpu_not_ready(void)
{
    int rval;

    boot_count++;
    LOG("cpu_not_ready: boot_count = %d, active_bios = %d",
        boot_count, active_bios);
    if (boot_count == 2) {
        LOG("Switching BIOS");
        boot_count = 0;
        if (gpio_line_bios_switch == NULL) {
            gpio_line_bios_switch = gpiod_chip_get_line(chip, GPIO_BIOS_SWITCH);
            if (!gpio_line_bios_switch) {
                LOG("cpu_not_ready: gpiod_chip_get_line failed for %d",
                    GPIO_BIOS_SWITCH);
                gpiod_chip_close(chip);
                return (-1);
            }

            rval = gpiod_line_request_output(gpio_line_bios_switch,
                "cpu_power_seq", GPIOD_LINE_ACTIVE_STATE_HIGH);
            if (rval != 0) {
                LOG("cpu_not_ready: gpiod_line_request_output() "
                    "failed GPIO_BIOS_SWITCH");
                gpiod_chip_close(chip);
                return (-1);
            }
        }

        rval = gpiod_line_get_value(gpio_line_bios_switch);
        if (rval < 0)
            LOG("cpu_not_ready: gpiod_line_get_value() bios status failed");
        LOG("cpu_not_ready: Switching to Golden BIOS Flash");
        gpiod_line_set_value(gpio_line_bios_switch, 1);
        active_bios = 1;
    }
    return (0);
}

/* before power on, check whether is bmc reboot instead of switch power cycle */
int pre_power(void)
{
    int rval;

 	rval = gpiod_line_request_bulk_input(&bulk, "cpu_power_seq");
 	if (rval != 0) {
        LOG("pre_power: gpiod_line_request_bulk_input() failed ");
        gpiod_chip_close(chip);
        return (-1);
 	}
    gpio_line_gpio_a4 = gpiod_chip_get_line(chip, GPIOA4);

    rval = power_on_wait(1000); // 1 second
 	if (rval != 0)
        return (rval);
    /* dram and all core power */
    if ((gpio_number & 0xC) == 0xC)
        bmc_reboot = 1;

    LOG("pre_power: gpio_number=%x, bmc_reboot=%d", gpio_number, bmc_reboot);
    gpiod_line_release_bulk(&bulk);
    gpiod_line_release(gpio_line_gpio_a4);
    gpio_line_gpio_a4 = NULL;
    return (0);
}

/* post power on action */
int post_power(void)
{
    if (active_bios) {
        gpiod_line_release(gpio_line_bios_switch);
        gpio_line_bios_switch = NULL;
        active_bios = 0;
    }
}

/* interrupt handler */
void int_handler(int sig)
{
    LOG("process exit - received Ctrl-C");
    exit(0);
}

/* devmem read/write routine */
int read_devmem(off_t target,unsigned long *read_value,unsigned long writeval) 
{
    int fd;
    void *map_base, *virt_addr;
    unsigned long read_result;
    int access_type = 'w';

    if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) {
        perror("error open /dev/mem");    
        exit (-1);
    }

    /* Map one page */
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
        fd, target & ~MAP_MASK);
    if (map_base == (void *) -1) {
        perror("error mmap()");    
        exit (-1);
    }
    virt_addr = map_base + (target & MAP_MASK);
    switch (access_type) {
        case 'b':
            read_result = *((unsigned char *) virt_addr);
            break;
        case 'h':
            read_result = *((unsigned short *) virt_addr);
            break;
        case 'w':
            read_result = *((unsigned long *) virt_addr);
            break;
        default:
            LOG("Illegal data type '%c'.", access_type);
            exit(2);
    }

    if (read_value != NULL)
        *read_value = read_result;
    if (read_value == NULL) {
        switch (access_type) {
            case 'b':
            *((unsigned char *) virt_addr) = writeval;
            read_result = *((unsigned char *) virt_addr);
            break;
        case 'h':
            *((unsigned short *) virt_addr) = writeval;
            read_result = *((unsigned short *) virt_addr);
            break;
        case 'w':
            *((unsigned long *) virt_addr) = writeval;
            read_result = *((unsigned long *) virt_addr);
            break;
        }
    }
    if (munmap(map_base, MAP_SIZE) == -1) {
        perror("error munmap()");    
        exit (-1);
    }
    close(fd);
    return 0;
}

#ifdef DEBUG
/* itimer debug handle */
void debug_handler (int signum)
{
    static int count = 0;
    struct stat sb;

    LOG("debug timer: count=%d", ++count);
    switch_console(CONSOLE_BMC);
}

/* enable debug interval timer */
void debug_set(int val)
{
    struct itimerval timer;

    timer.it_value.tv_sec = val;
    timer.it_value.tv_usec = 0;
    timer.it_interval = timer.it_value;

    if (setitimer (ITIMER_REAL, &timer, NULL) == -1) {
        perror("error calling setitimer()");    
    }
}
#endif
