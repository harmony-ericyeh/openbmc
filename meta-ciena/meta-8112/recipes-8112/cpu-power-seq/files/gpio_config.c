/* Copyright (c) 2021 Ciena Corporation, All Rights Reserved */
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <gpiod.h>
#include <signal.h>
#include <string.h>

#define GPIO_109 109
#define GPIO_107 107
#define GPIO_102 102

/* log */
#define FFLUSH  { fflush(stdout); fflush(stderr); }
#define LOG_SIZE 256    
char log_buf[LOG_SIZE];
#define LOG(...) {                               \
    snprintf(log_buf, LOG_SIZE, ## __VA_ARGS__); \
    printf("%s\n", log_buf);                     \
    FFLUSH;                                      \
}

struct gpiod_chip *chip;
struct gpiod_line *gpio_109, *gpio_107, *gpio_102;

int  gpio_init(void);

/*
 * main
 */
int main(int argc, char *argv[])
{
    int rval;

    /* init gpios */
    rval = gpio_init();
    if (rval != 0)
        return (rval);

    while (1) {
        sleep(600);
    }

    exit (0);
}

/* initialize gpio setting */
int gpio_init(void)
{
    int rval;

    chip = gpiod_chip_open("/dev/gpiochip0");
    if (!chip) {
       LOG("failure: gpiod_chip_open()");
       return (-1);
    }

    /* GPIO_109 */
    gpio_109 = gpiod_chip_get_line(chip, GPIO_109);
    if (!gpio_109) {
        LOG("Error: gpiod_chip_get_line failed for GPIO_109");
        gpiod_chip_close(chip);
        return (-1);
    }
    rval = gpiod_line_request_output(gpio_109,
        "gpio_config", GPIOD_LINE_ACTIVE_STATE_HIGH);
    if (rval != 0) {
        LOG("Error: gpiod_line_request_output() failed for GPIO_109");
        gpiod_chip_close(chip);
        return (-1);
    }

    /* GPIO_107 */
    gpio_107 = gpiod_chip_get_line(chip, GPIO_107);
    if (!gpio_107) {
        LOG("Error: gpiod_chip_get_line failed for GPIO_107");
        gpiod_chip_close(chip);
        return (-1);
    }
    rval = gpiod_line_request_output(gpio_107,
        "gpio_config", GPIOD_LINE_ACTIVE_STATE_HIGH);
    if (rval != 0) {
        LOG("Error: gpiod_line_request_output() failed for GPIO_107");
        gpiod_chip_close(chip);
        return (-1);
    }

    /* GPIO_102 */
    gpio_102 = gpiod_chip_get_line(chip, GPIO_102);
    if (!gpio_102) {
        LOG("Error: gpiod_chip_get_line failed for GPIO_102");
        gpiod_chip_close(chip);
        return (-1);
    }
    rval = gpiod_line_request_output(gpio_102,
        "gpio_config", GPIOD_LINE_ACTIVE_STATE_HIGH);
    if (rval != 0) {
        LOG("Error: gpiod_line_request_output() failed for GPIO_102");
        gpiod_chip_close(chip);
        return (-1);
    }

    LOG("Driving GPIO_109, GPIO_107, GPIO_102 low");
    gpiod_line_set_value(gpio_109, 0);
    gpiod_line_set_value(gpio_107, 0);
    gpiod_line_set_value(gpio_102, 0);

    return (0);
}
