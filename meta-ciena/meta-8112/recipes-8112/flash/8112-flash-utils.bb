
SUMMARY = "Ciena 8112 Flashing Utilities"
DESCRIPTION = "Application to support flashing utilities on 8112 platform"
PR = "r0"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

RDEPENDS_${PN} = "bash"

SRC_URI += "\
            file://flash_bios.sh \
           "

S = "${WORKDIR}"
ROOT = "${STAGING_DIR_TARGET}"

do_install_append() {
    install -d ${D}/usr/sbin
    install -m 0755 ${S}/flash_bios.sh ${D}/${sbindir}/flash_bios.sh
}
