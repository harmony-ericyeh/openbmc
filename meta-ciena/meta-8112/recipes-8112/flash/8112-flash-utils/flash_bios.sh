#!/bin/bash
#
# flash_bios.sh
# 

usage () {
	echo "Usage: $(basename $0) [ -k ] <BIOS image file>"
    echo "       -k     : Update Known Good Image"
    echo "       default: Update Current Image" 
    exit 1
}

[ $# -eq 0 ] && usage

IMAGE="$1"
kflag=0
if [ $1 == "-k" ]; then
    [ $# -eq 1 ] && usage
    kflag=1
    IMAGE="$2"
fi

if [ ! -f $IMAGE ]; then
	echo $IMAGE
	echo "The image file $IMAGE does not exist"
	exit 1
fi

host_to_bmc () {
    gpioset --mode=signal 0 149=1 &
    [ $kflag -eq 1 ] && gpioset --mode=signal 0 150=1 &
    sleep 1
}

bmc_to_host () {
    PID=`ps | grep gpioset | grep "149=" | awk '{ print $1}'`
    [ -n "$PID" ] && kill -9 $PID
    PID=`ps | grep gpioset | grep "150=" | awk '{ print $1}'`
    [ -n "$PID" ] && kill -9 $PID
    gpioset 0 149=0
    gpioset 0 150=0
}

do_flash () {
	echo "--- Bind the ASpeed SMC driver"
	echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/bind
	sleep 1

    grep "BIOS" /proc/mtd
	HOST_MTD=$(cat /proc/mtd | grep "BIOS" | sed -n 's/^\(.*\):.*/\1/p')
	if [ -z "$HOST_MTD" ]; then
		echo "Fail to probe BIOS SPI Flash"
        bmc_to_host
		exit 1
	fi

	echo "--- Flashing firmware to @/dev/$HOST_MTD"
	flash_eraseall /dev/$HOST_MTD
	flashcp -v $IMAGE /dev/$HOST_MTD

	echo "--- Unbind the ASpeed SMC driver"
	echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/unbind
}

# Unbind bios flash 
if [ -e /sys/bus/platform/drivers/aspeed-smc/1e630000.spi ]; then
    echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/unbind
    sleep 1
fi

# Switch the host SPI bus to BMC"
echo "--- Switch the host SPI bus to BMC."
host_to_bmc

# Flash the firmware
do_flash

# Switch the host SPI bus to HOST."
echo "--- Switch the host SPI bus to HOST."
bmc_to_host

if [[ $? -ne 0 ]]; then
	echo "ERROR: Switch the host SPI bus to HOST. Please check gpio state"
	exit 1
fi

exit 0
