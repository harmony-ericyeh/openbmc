
SUMMARY = "OpenBMC Upgrade"
DESCRIPTION = "OpenBMC upgrade with IPMI command and USB interface"
PR = "r0"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"
SRC_URI_append = " file://obmc_hostipmi.sh \
                   file://obmc_hostlistener.sh \
                   file://obmc_installer.sh \
                   file://obmc_postinstaller.sh \
                   file://8112rcscript.sh \
                   file://obmc_hostlistener.service \
                   file://8112rcscript.service \
                   file://code_update.sh \
                 "

inherit systemd

DEPENDS += "systemd"
RDEPENDS_${PN} += "libsystemd"
RDEPENDS_${PN} += "bash"

do_install() {
    install -d ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/obmc_hostipmi.sh ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/obmc_hostlistener.sh ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/obmc_installer.sh ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/obmc_postinstaller.sh ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/8112rcscript.sh ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/code_update.sh ${D}/${sbindir}

    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/obmc_hostlistener.service \
        ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/8112rcscript.service \
        ${D}${systemd_unitdir}/system
}

SYSTEMD_SERVICE_${PN}_append = "obmc_hostlistener.service 8112rcscript.service"
