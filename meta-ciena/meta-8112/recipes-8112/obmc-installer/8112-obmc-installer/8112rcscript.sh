#!/bin/sh
#
# 8112rcscript.sh
#
# Use this script for place holder of 8112 startup tasks.
# - Create eMMC partition first time system bootup.
#   o no argument passed
#   o partition 7 will be created
#   o partition start address is 4M align from the previous partition (6)
#   o partition size is 2014M
#
NAME=config
NUM=7
MMC=/dev/mmcblk0
DEV=/dev/mmcblk0p${NUM}
MNT=/var/config

align()
{
    _v=$1
    _a=$((_v+4))
    _b=$((_a/4))
    _c=$((_b*4))
    echo $_c
}

do_mkfs()
{
    mkfs.ext4 -F $DEV  > /dev/null 2>&1
    e2label $DEV $NAME > /dev/null 2>&1
}

# config log level
echo 4 > /proc/sys/kernel/printk

# mount tmpfs for upgrade
[ -d /var/obmc ] || mkdir -p /var/obmc
mount -t tmpfs -o mode=755,nodev -o size=320M tmpfs /var/obmc

# create partition on the first time
[ -d $MNT ] || mkdir $MNT

# partition has been created
printf "Fix\n" | parted ---pretend-input-tty $MMC print | grep $NAME
if [ $? -eq 0 ]; then
    fsck.ext4 -p $DEV
    mount $DEV $MNT -t ext4 -o rw

    # re-mkfs if partition becomes unmountable
    if [ $? -ne 0 ]; then
        do_mkfs
        mount $DEV $MNT -t ext4 -o rw
    fi
    exit 0
fi

# partition start and end
last=$(parted $MMC -- print | grep rwfs | awk '{print $3}' | sed -e 's/MB//')
start=`align $last`
end=$((start+2014))

echo "Create partion $DEV - start $start, end $end"
parted $MMC -- mkpart $NAME ext4 $start $end
parted $MMC -- name $NUM $NAME
parted $MMC -- print
do_mkfs
mount $DEV $MNT -t ext4 -o rw
exit 0
