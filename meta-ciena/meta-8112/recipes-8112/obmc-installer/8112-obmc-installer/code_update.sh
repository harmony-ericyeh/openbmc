#!/bin/bash
#
# core_update.sh
#
# Number  Start   End     Size    File system  Name     Flags
#  1      20.5kB  1069kB  1049kB               primary  msftdata
#  2      1069kB  34.6MB  33.6MB  ext4         boot-a
#  3      34.6MB  68.2MB  33.6MB  ext4         boot-b
#  4      68.2MB  337MB   268MB   ext4         rofs-a
#  5      337MB   605MB   268MB   ext4         rofs-b
#  6      605MB   1073MB  468MB   ext4         rwfs
#

# Default image path in BMC
BMC_FIMG="/tmp/fitImage"
BMC_ROFS="/tmp/rofs"
reboot_local=1
overlay_list=""

usage () {
    echo "$*"
    echo "Usage: $(basename $0) -u <u-boot>"
    echo "  OR"
    echo "Usage: $(basename $0) [ -n -o <file> ] -b <fitImage> -r <roofs_ext4>"
    echo "       -n        : no reboot"
    echo "       -o <file> : file in /etc lower dir to be copied to upper dir"
    exit 1
}

while getopts "b:r:hnu:o:" option; do
    case ${option} in
    b ) bootImage="$OPTARG"
        ;;
    r ) rofsImage="$OPTARG"
        ;;
    h ) usage
        ;;
    n ) reboot_local=0
        ;;
    u ) uboot="$OPTARG"
        ;;
    o ) overlay_list="$overlay_list $OPTARG"
        ;;
    * ) echo "Invalid option"
        usage
        ;;
    esac
done

# spi flash bmc upgrade option
dev_uboot=/dev/mtd/u-boot
if [ -n "$uboot" ]; then
    [ ! -f $uboot ] && usage "Invalid image input - $uboot"
    flashcp $uboot $dev_uboot
    exit 0
fi

# use default path when invoke remotely
if [ $# -eq 0 ]; then
    bootImage="$BMC_FIMG"
    rofsImage="$BMC_ROFS"
    reboot_local=0
fi

# emmc upgrade options
[ -z "$bootImage" -o -z "$rofsImage" ] && usage "Invalid options: $*"
[ ! -f $bootImage -o ! -f $rofsImage ] && \
    usage "Invalid image input - $bootImage, $rofsImage"

# definition of boot and rofs filesystem
case $(fw_printenv bootside | cut -d'=' -f2) in
b ) PART=a
    BOOT_LABEL=boot-a; BOOT_DEV=/dev/mmcblk0p2
    ROFS_LABEL=rofs-a; ROFS_DEV=/dev/mmcblk0p4
    ;;
a ) PART=b
    BOOT_LABEL=boot-b; BOOT_DEV=/dev/mmcblk0p3
    ROFS_LABEL=rofs-b; ROFS_DEV=/dev/mmcblk0p5
    ;;
* ) echo "Stop code update - unknown bootside setting"
    fw_printenv bootside
    exit 1
    ;;
esac

copy_overlay ()
{
    [ -z "$overlay_list" ] && return
    _mnt=/tmp/overlay
    mkdir $_mnt
    mount $ROFS_DEV $_mnt
    [ $? -ne 0 ] && usage "Error mounting $ROFS_DEV"

    for _file in $overlay_list; do
        echo "copy $_file"
        [ -f $_mnt/$_file ] && $ECHO cp $_mnt/$_file $_file
    done
    umount $_mnt
    rmdir $_mnt
}

echo
echo "Code update start ... "

# update boot filesystem
echo "Mounting $BOOT_LABEL ..."
BOOT_MNT=/tmp/boot_mnt
BOOT_IMAGE=$BOOT_MNT/fitImage
mkdir $BOOT_MNT
mount $BOOT_DEV $BOOT_MNT
[ $? -ne 0 ] && usage "Error mounting $BOOT_LABEL"

echo "Install $bootImage to $BOOT_IMAGE ..."
[ -f $BOOT_IMAGE ] && cp $BOOT_IMAGE ${BOOT_IMAGE}_OLD
cp $bootImage $BOOT_IMAGE
umount $BOOT_MNT
rmdir $BOOT_MNT

# update rofs filesystem
echo "Install $rofsImage to $ROFS_LABEL ..."
dd if=$rofsImage of=$ROFS_DEV bs=8K
fsck.ext4 -p $ROFS_DEV
e2label $ROFS_DEV $ROFS_LABEL

# setup u-boot env
echo "Update bootside env to $PART ..."
fw_setenv bootside $PART
fw_printenv bootside

# copy /etc overlay lower dir files to upper dir
echo "Copy /etc overlay lower dir files to upper dir"
copy_overlay

echo "Code Update done."
echo

# reboot when invoke locally
if [ $reboot_local -eq 1 ]; then
    sync
    umount -a
    i2cset -y 5 0x20 0x12 0x8
    sleep 3
    systemctl reboot
fi

exit 0
