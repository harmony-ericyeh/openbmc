#!/bin/sh
#
# obmc_hostipmi.sh
#
# The ipmid passes ipmi raw(6) command from SAOS to this script.
# Instead of modify ipmid, use this script to support obmc upgrade
# related requests. Observe retry happened when the operation doesn't
# complete within short period of time (10-20 seconds).  Signal the
# obmc_hostlistener service to handle upgrade task.
#
tmp_dir=/var/obmc
img=$tmp_dir/obmc.tar.gz
dir=/var/config
board_version=$dir/board.version
debug_enable=$dir/debug.enable
cpld_skip=$dir/cpld.skip
cpld_update=$dir/cpld.update
upgrade_status=/tmp/upgrade.status
usb=/tmp/usb

opcode=$1
arg=$2
ret=0

echo $(basename $0) $*
if [ ! -d $dir ]; then
    echo "$dir not found"
    exit -1
fi

cd $dir
case $opcode in
0)
    # To inform BMC to download image from USB interface.
    #   Arg    : Image size in Mbyte
    #   Return : None
    echo -n "[$opcode $arg] - download"
    dd if=$usb of=$img bs=1M count=$arg
    ret=$?
    ;;
1)
    # To inform BMC to do upgrade.
    #   Arg    : None
    #   Return : None
    echo -n "[$opcode $arg] - trigger upgrade"
    pids=`ps | grep obmc_hostlist | grep -v grep | awk '{print $1}'`
    kill -USR1 $pids
    ret=$?
    ;;
2)
    # To send board version and get current CPLD version.
    #   Arg    : Board version - 1 (MAX2); 2 (MAX10)
    #   Return : CPLD current version
    echo -n "[$opcode $arg] - cpld & board version"
    echo "BP1 $arg" > $board_version
    cpld_ver=`cpld_version.sh`
    [ -z "$cpld_ver" ] && cpld_ver="0"
    ret=$(printf "%d" 0x${cpld_ver})
    ;;
3)
    # To get upgrade status.
    #   Arg    : None
    #   Return : 0 - upgrade done; None 0 - upgrade in progress
    echo -n "[$opcode $arg] - upgrade status"
    if [ -f $upgrade_status ]; then
        ret=`cat $upgrade_status`
    else
        ret=-1
    fi
    ;;
10)
    # To enable/disable installer debug mode (for debug).
    #   Arg    : 0 - Enable; None 0 - Disable
    #   Return : None
    echo -n "[$opcode $arg] - debug mode"
    rm -f $debug_enable
    [ "$arg" = "0" ] && touch $debug_enable
    ret=0
    ;;
11)
    # To enable/disable skip cpld upgrade (for debug).
    #   Arg    : 0 - Enable; None 0 - Disable
    #   Return : None
    echo -n "[$opcode $arg] - cpld skip"
    rm -f $cpld_skip
    [ "$arg" = "0" ] && touch $cpld_skip
    ret=0
    ;;
12)
    # To enable/disable force cpld update (for debug).
    #   Arg    : 0 - Enable; None 0 - Disable
    #   Return : None
    echo -n "[$opcode $arg] - cpld update"
    rm -f $cpld_update
    [ "$arg" = "0" ] && touch $cpld_update
    ret=0
    ;;
*)
    echo -n [$opcode $arg] - unknown
    ret=-1
esac

echo ", ret $ret"

exit $ret
