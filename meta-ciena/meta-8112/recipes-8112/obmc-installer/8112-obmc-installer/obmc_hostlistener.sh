#!/bin/sh
#
# obmc_hostlistener.sh
#
# Signaled by obmc_hostipmi to handle the upgrade operation.
#
cmd=$(basename $0)
dir=/var/config
tmp_dir=/var/obmc
img=$dir/obmc.tar.gz
tmp_img=$tmp_dir/obmc.tar.gz
installer=obmc_installer.sh
upgrade_status=/tmp/upgrade.status
status_error="9"

installer_sym=$dir/obmc_installer.sh
version=$dir/obmc.version
tmp_installer_sym=$tmp_dir/obmc_installer.sh
tmp_version=$tmp_dir/obmc.version
rm_list="$installer_sym $version $tmp_installer_sym $tmp_version $upgrade_status"

log()
{
    echo $* | systemd-cat -t $cmd
}

pre_clean()
{
    rm -f $dir/oBMC_*
    rm -f $dir/oCPLD_*
    rm -f $tmp_dir/oBMC_*
    rm -f $tmp_dir/oCPLD_*
    rm -f $rm_list
}

do_upgrade()
{
    log "OpenBMC Upgrade Start"
    pre_clean
    if [ ! -f $img -a ! -f $tmp_img ]; then
        log "Image not found - img $img, tmp_img $tmp_img"
        echo "$status_error" > $upgrade_status
        return
    fi

    # uncompress and untar image
    if [ -f $tmp_img ]; then
        log "Uncompress image: tar zxvf $tmp_img"
        cd $tmp_dir
        tar zxvf $tmp_img
        if [ $? -ne 0 ]; then
            log "Fail uncompress image"
            echo "$status_error" > $upgrade_status
        fi
    else
        log "Uncompress image: tar zxvf $img"
        cd $dir
        tar zxvf $img
        if [ $? -ne 0 ]; then
            log "Fail uncompress image"
            echo "$status_error" > $upgrade_status
        fi
    fi

    # run installer
    # When installer isn't exist because of uncompress failure,
    # the upgrade will be timeout then host will retry.
    log "Invoke OpenBMC Installer - $installer"
    cd /
    if [ -f $tmp_dir/$installer ]; then
        $tmp_dir/$installer
    else
        if [ -f $dir/$installer ]; then
            cp $dir/$installer /tmp
            /tmp/$installer
        fi
    fi
}

# main()
trap do_upgrade 10 12

log "OpenBMC Host Listener"
while :;
do
    read x < /dev/fd/1 | echo > /dev/null
done
