#!/bin/sh
cmd=$(basename $0)
update=/usr/sbin/code_update.sh
dir=/var/config
tmp_dir=/var/obmc
img=$dir/obmc.tar.gz
tmp_img=$tmp_dir/obmc.tar.gz
backup=$dir/backup

cpld_skip=$dir/cpld.skip
cpld_update=$dir/cpld.update
cpld_dir=/var/cpld
cpld_name=/var/cpld/name

overlay_list="/etc/passwd /etc/shadow"

# installer debug
ECHO=""
debug_enable=$dir/debug.enable
[ -f $debug_enable ] && ECHO=echo

# keep multiple logs
backup_log() {
    [ ! -d $backup  ] && mkdir $backup
    ls $dir/log_* > /dev/null 2>&1
    [ $? -eq 0 ] && mv $dir/log_* $backup
    line=$(ls -1 ${backup}/ | wc -l)
    [ $line -ne 0 -a $line -gt 16 ] && rm $(ls -1 ${backup}/* | head -n 1)
}
backup_log
outfile=$dir/log_$(date +"%Y%m%d%H%M%S")
touch $outfile

# oBMC image
if [ -f $tmp_dir/obmc.version ]; then
    uboot=$(ls $tmp_dir/oBMC_*.u-boot 2> /dev/null)
    fitimage=$(ls $tmp_dir/oBMC_*.fit 2> /dev/null)
    rootfs=$(ls $tmp_dir/oBMC_*.ext4 2> /dev/null)
    rootfs_gz=$(ls $tmp_dir/oBMC_*.ext4.gz 2> /dev/null)
    installer=$(ls $tmp_dir/oBMC_*.obmc_installer.sh 2> /dev/null)
    installer_sym=$tmp_dir/obmc_installer.sh
    version=$tmp_dir/obmc.version
else
    uboot=$(ls $dir/oBMC_*.u-boot 2> /dev/null)
    fitimage=$(ls $dir/oBMC_*.fit 2> /dev/null)
    rootfs=$(ls $dir/oBMC_*.ext4 2> /dev/null)
    rootfs_gz=$(ls $dir/oBMC_*.ext4.gz 2> /dev/null)
    installer=$(ls $dir/oBMC_*.obmc_installer.sh 2> /dev/null)
    installer_sym=$dir/obmc_installer.sh
    version=$dir/obmc.version
fi
rm_list_obmc="$img $tmp_img $uboot $fitimage $rootfs $rootfs_gz $installer $installer_sym $version $debug_enable"

# cpld image
if [ -f $tmp_dir/obmc.version ]; then
    cpld_max2=$(ls $tmp_dir/oCPLD_*_MAX2.img 2> /dev/null)
    cpld_max10=$(ls $tmp_dir/oCPLD_*_MAX10.img 2> /dev/null)
else
    cpld_max2=$(ls $dir/oCPLD_*_MAX2.img 2> /dev/null)
    cpld_max10=$(ls $dir/oCPLD_*_MAX10.img 2> /dev/null)
fi
board_version=$dir/board.version
rm_list_cpld="$cpld_max2 $cpld_max10 $board_version"
rm_list="$rm_list_obmc $rm_list_cpld $cpld_skip $cpld_update"

# upgrade status
status_done="0"
status_start="1"
status_uboot="2"
status_emmc="3"
status_cpld="4"
status_error="9"
upgrade_status=/tmp/upgrade.status

# eMMC partition
BOOT_LABEL=""
ROFS_LABEL=""
BOOT_DEV=""
ROFS_DEV=""
PART=""

status() 
{
    echo $1 > $upgrade_status
}

log()
{
    echo $(date +"%Y_%m%d_%H:%M:%S") "$*" >> $outfile
}

dump_log()
{
    log "Log 8112rcscript journal"
    journalctl -u 8112rcscript >> $outfile
    log "Log obmc_hostlistener journal"
    journalctl -u obmc_hostlistener >> $outfile
    log "Log all journal"
    journalctl >> $outfile
    log "Log dmesg"
    dmesg >> $outfile
}

error_exit()
{
    $ECHO rm -f $rm_list
    [ -z "$ECHO" ] && echo "" > /sys/kernel/config/usb_gadget/mass-storage/UDC
    $ECHO dump_log
    $ECHO sync
    $ECHO umount -a
    $ECHO exit 1
}

check_image()
{
    log "uboot     : $uboot"
    log "fitimage  : $fitimage"
    log "rootfs    : $rootfs"
    log "rootfs_gz : $rootfs_gz"

    _err=0
    if [ ! -f "$version" ]; then
        log "Missing $version"
        _err=1
    fi
    if [ -z "$uboot" ]; then
        log "Missing u-boot image"
        _err=1
    fi
    if [ -z "$fitimage" ]; then
        log "Missing fit image"
        _err=1
    fi
    if [ -z "$rootfs" -a -z "$rootfs_gz" ]; then
        log "Missing rootfs"
        _err=1
    fi
    if [ $_err -ne 0 ]; then
        log "Exit installer"
        status "$status_error"
        error_exit
    fi
}

update_passwd ()
{
    grep diag /etc/passwd
    [ $? -eq 0 ] && return

    case $(fw_printenv bootside | cut -d'=' -f2) in
        a ) ROFS_LABEL=rofs-a; ROFS_DEV=/dev/mmcblk0p4 ;;
        b ) ROFS_LABEL=rofs-b; ROFS_DEV=/dev/mmcblk0p5 ;;
        * ) log "unknown bootside setting"
            fw_printenv bootside
            return
            ;;
    esac

    _mnt=/tmp/overlay
    $ECHO mkdir $_mnt
    $ECHO mount $ROFS_DEV $_mnt
    [ $? -ne 0 ] && usage "Error mounting $ROFS_DEV"

    for _file in $overlay_list; do
        log "copy $_file"
        [ -f $_mnt/$_file ] && $ECHO cp $_mnt/$_file $_file
    done
    $ECHO umount $_mnt
    $ECHO rmdir $_mnt
}

upgrade_cpld()
{
    log "cpld_max2 : $cpld_max2"
    log "cpld_max10: $cpld_max10"

    _err=0
    if [ ! -f "$board_version" ]; then
        log "Missing $board_version"
        _err=1
    fi
    if [ -z "$cpld_max2" ]; then
        log "Missing cpld MAX2"
        _err=1
    fi
    if [ -z "$cpld_max10" ]; then
        log "Missing cpld MAX10"
        _err=1
    fi
    [ $_err -ne 0 ] && return

    brd=$(grep BP1 $board_version | awk '{print $2}')
    if [ -z "$brd" ]; then
        log "No BP1 in $board_version"
        return
    fi
    case $brd in
        1) cpld="$cpld_max2"  ; type="MAX2"  ;;
        2) cpld="$cpld_max10" ; type="MAX10" ;;
        *) log "No valid board version %brd"; return;;
    esac
    str=$(grep $type $version | awk '{print $2}' | cut -f2 -d.)
    if [ -z "$str" ]; then
        log "No $type in $version"
        return
    fi
    file=$(printf "%d" 0x${str})
    str=`cpld_version.sh`
    if [ -z "$str" ]; then
        log "Cannot find CPLD version"
        return
    fi
    cur=$(printf "%d" 0x${str})

    log "CPLD version of the file $file, current $cur"

    # if cpld.skip is found, skip the upgrade
    if [ -f $cpld_skip ]; then
        log "Skip CPLD Upgrade, find $cpld_skip"
        return
    fi
    # if cpld.update is found, do update
    if [ $file -gt $cur -o -f $cpld_update ] ;then
        # Stage cpld image for upgrade during next reset
        log "Update CPLD: $cpld"
        rm -rf $cpld_dir
        mkdir -p $cpld_dir
        cp $cpld $cpld_dir
        echo "$(basename $cpld)" > $cpld_name
    fi
}

upgrade_precheck ()
{
    # sync bootside env to /root
    _root=$(df / | grep rofs- | cut -d'-' -f3)
    _env=$(fw_printenv bootside | cut -d'=' -f2)
    if [ "$_root" = "$_env" ]; then
        str="$_env"
    else
        # adjust bootside to be the same as /root
        echo "sync bootside env to $_root"
        $ECHO fw_setenv bootside $_root
        str="$_root"
    fi

    # define upgrade partitions
    case "$str" in
    b ) BOOT_LABEL="boot-a"; BOOT_DEV="/dev/mmcblk0p2"
        ROFS_LABEL="rofs-a"; ROFS_DEV="/dev/mmcblk0p4"
        PART="a"
        ;;
    a ) BOOT_LABEL="boot-b"; BOOT_DEV="/dev/mmcblk0p3"
        ROFS_LABEL="rofs-b"; ROFS_DEV="/dev/mmcblk0p5"
        PART="b"
        ;;
    esac

    # check fs and restore label
    $ECHO fsck.ext4 -p $BOOT_DEV
    $ECHO fsck.ext4 -p $ROFS_DEV
    blkid | grep $BOOT_LABEL
    if [ $? -ne 0 ]; then
        echo "Restore label - $BOOT_DEV $BOOT_LABEL"
        $ECHO e2label $BOOT_DEV $BOOT_LABEL
    fi
    blkid | grep $ROFS_LABEL
    if [ $? -ne 0 ]; then
        echo "Restore label - $ROFS_DEV $ROFS_LABEL"
        $ECHO e2label $ROFS_DEV $ROFS_LABEL
    fi
}

upgrade_emmc ()
{
    # update boot filesystem
    echo "Mounting $BOOT_LABEL ..."
    BOOT_MNT=/tmp/boot_mnt
    BOOT_IMAGE=$BOOT_MNT/fitImage
    $ECHO mkdir $BOOT_MNT
    $ECHO mount $BOOT_DEV $BOOT_MNT
    if [ $? -ne 0 ]; then
        echo "Error mounting $BOOT_LABEL"
        exit 1
    fi

    echo "Install $fitimage to $BOOT_IMAGE ..."
    [ -f $BOOT_IMAGE ] && cp $BOOT_IMAGE ${BOOT_IMAGE}_OLD
    $ECHO cp $fitimage $BOOT_IMAGE
    $ECHO umount $BOOT_MNT
    $ECHO rmdir $BOOT_MNT

    # update rofs filesystem
    if [ -n "$rootfs_gz" ]; then
        echo "Install $rootfs_gz to $ROFS_LABEL ..."
        [ -z "$ECHO" ] && gunzip -c -d $rootfs_gz | dd of=$ROFS_DEV bs=8K
    else
        echo "Install $rootfs to $ROFS_LABEL ..."
        $ECHO dd if=$rootfs of=$ROFS_DEV bs=8K
    fi
    $ECHO fsck.ext4 -p $ROFS_DEV
    $ECHO e2label $ROFS_DEV $ROFS_LABEL

    # setup u-boot env
    echo "Update bootside env to $PART ..."
    $ECHO fw_setenv bootside $PART
    $ECHO fw_printenv bootside
}

# check obmc_hostlistener status
# If reports error, notify host as upgrade complete
# so the upgrade will be triggered again after device reset.
log "Check obmc_hostlistener status"
if [ -f $upgrade_status ]; then
    stat=$(cat $upgrade_status)
    if [ "$stat" = "$status_error" ]; then
        log "Error from obmc_hostlistener, exit"
        error_exit
    fi
    rm -f $upgrade_status
fi

# upgrade start
log "OpenBMC Installer Start ..."
status "$status_start"

# sanity check - check image list
log "Check images"
check_image

# upgrade u-boot
log "Upgrade u-boot Image: $update -u $uboot"
status "$status_uboot"
$ECHO $update -u $uboot >> $outfile

# check upgrade partitions
log "Check eMMC upgrade partitions"
upgrade_precheck

# upgrade eMMC
log "Update eMMC Image"
status "$status_emmc"
upgrade_emmc

# update passwd
log "Update password"
update_passwd

# upgrade CPLD
log "Upgrade CPLD ..."
status "$status_cpld"
upgrade_cpld

# cleanup
log "Clean up - $rm_list"
$ECHO rm -f $rm_list

# Release the usb to notify the host that upgrade is done.
log "Release USB mass-storage"
[ -z "$ECHO" ] && echo "" > /sys/kernel/config/usb_gadget/mass-storage/UDC
status "$status_done"

# collect log
dump_log

sync
$ECHO umount -a
sleep 2

# done upgrade
# HOST will reset the whole board.

exit 0
