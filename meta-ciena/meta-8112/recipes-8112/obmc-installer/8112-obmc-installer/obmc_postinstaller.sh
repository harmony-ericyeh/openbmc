#!/bin/sh
cmd=$(basename $0)
cpld_dir=/var/cpld
cpld_name=/var/cpld/name
cpld_log=/var/log/cpld.log

if [ -f ${cpld_log} ]; then
    val=$(du -sk $cpld_log | awk '{print $1}')
    [ $val -ge 32 ] && (mv ${cpld_log} ${cpld_log}.old; touch $cpld_log)
else
    touch $cpld_log
fi

log()
{
    echo $(date +"%Y_%m%d_%H:%M:%S") "$*" >> $cpld_log
}

clean_up()
{
    rm -rf $cpld_dir
}

display_msg()
{
    msgfile=/tmp/cpld_msg
    console=/dev/console
cat > ${msgfile} << EOF

#######################################
#   Updating CPLD. Don't power off.   #
#######################################

EOF
    sh -c "while : ; do cat ${msgfile} > ${console} ; sleep 90; done" &
    sh -c "sleep 12; cat ${msgfile} > ${console}" &
}

gpio_info()
{
    str=$(gpioget 0 40 41)
    [ -z "$str" ] && str="Device or resource busy"
    log "BMC status 0 1          : $str"

    str=$(gpioget 0 168 134 69 128)
    [ -z "$str" ] && str="Device or resource busy"
    log "CPU boot signal 0 1 2 3 : $str"

    str=$(gpioget 0 172 70 135)
    [ -z "$str" ] && str="Device or resource busy"
    log "CPU boot signal 4 5 6   : $str"
}

stop_service()
{
    echo 4 > /proc/sys/kernel/printk
    systemctl stop systemd-journald-dev-log.socket
    systemctl stop systemd-journald.socket
    systemctl stop systemd-journald
    systemctl stop systemd-networkd.socket
    systemctl stop systemd-networkd
}

upgrade_cpld()
{
    cpld=$1
    log "Update CPLD: $cpld"
    stop_service

    gpioset --mode=signal 0 104=1 &
    sleep 2
    log "run: svf -n /dev/aspeed-jtag0 -p $cpld"
    svf -n /dev/aspeed-jtag0 -p $cpld >> $cpld_log 2>&1
    log "svf return $?"
}

# start
log "### Start $cmd ... ###"
if [ ! -f $cpld_name ]; then
    log "No need to upgrade CPLD, exit."
    exit 0
fi

# check
gpio_info
image=$(cat $cpld_name)
cpld="${cpld_dir}/${image}"
if [ ! -f $cpld ]; then
    log "Cannot find $cpld, exit."
    clean_up
    exit 0
fi

# indicate bmc is up
gpioset --mode=signal 0 40=0 &
gpioset --mode=signal 0 41=0 &

# message to console
display_msg

# upgrade
cp $cpld /tmp
clean_up
upgrade_cpld "/tmp/$image"

# reset
log "reset the whole board"
sync
umount -a
i2cset -y 5 0x20 0xe 1

sleep 10
exit 0
