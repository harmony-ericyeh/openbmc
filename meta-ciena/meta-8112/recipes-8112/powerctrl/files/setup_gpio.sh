#!/bin/bash

# Wait for GPIOF2 to be in normal condition

GPIOF2=`gpioget 0 42`
while [ $GPIOF2 -eq 0 ]
do
  GPIOF2=`gpioget 0 42`
  sleep 5
done
echo $GPIOF2
exit 0;
