PR = "r1"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10 "

inherit systemd
#inherit obmc-phosphor-systemd
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} += "rtc-config.service"


DEPENDS = "systemd"
RDEPENDS_${PN} = "bash"

SRC_URI = " file://rtc-config.service"

S = "${WORKDIR}"

do_install() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${S}/rtc-config.service ${D}${systemd_system_unitdir}
}

