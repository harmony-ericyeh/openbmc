FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://journald.conf "

do_install_append() {
    install -m 0644 ${WORKDIR}/journald.conf ${D}${sysconfdir}/systemd
}
