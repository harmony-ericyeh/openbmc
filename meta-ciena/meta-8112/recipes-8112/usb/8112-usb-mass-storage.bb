
SUMMARY = "Enable USB channel between Host and BMC"
DESCRIPTION = "There is a USB signal connect HOST CPU to BMC(AST2600 SOC).This enables this USB channel to send file image from host side to BMC. BMC side will be slave (vhub)"
PR = "r0"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"
SRC_URI_append = " file://host_bmc_usb_enable.sh"
SRC_URI_append = " file://host_bmc_usb_enable.service"

inherit systemd

DEPENDS += "systemd"
RDEPENDS_${PN} += "libsystemd"
RDEPENDS_${PN} += "bash"

do_install() {
    install -d ${D}/${sbindir}
    install -m 0755 ${WORKDIR}/host_bmc_usb_enable.sh ${D}/${sbindir}

    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/host_bmc_usb_enable.service \
        ${D}${systemd_unitdir}/system
}

SYSTEMD_SERVICE_${PN}_append = "host_bmc_usb_enable.service"

