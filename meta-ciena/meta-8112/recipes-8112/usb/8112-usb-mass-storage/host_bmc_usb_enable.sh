#!/bin/sh
create_dir()
{
   mkdir ${1} 2>/dev/null || { echo "mkdir failed for :${1}"; exit; }
}

if [ -d /sys/kernel/config/usb_gadget ]; then
   dd if=/dev/zero of=/tmp/usb bs=1M count=64
   cd /sys/kernel/config/usb_gadget
   create_dir mass-storage
   cd mass-storage
   echo "0x1d6b" >idVendor
   echo "0x0199" >idProduct
   create_dir strings/0x409
   echo "0123456789" > strings/0x409/serialnumber
   echo "Ciena Corp." > strings/0x409/manufacturer
   echo "Bar Gadget" > strings/0x409/product
   create_dir configs/c.1
   create_dir configs/c.1/strings/0x409
   echo "config 1" > configs/c.1/strings/0x409/configuration
   create_dir functions/mass_storage.usb0
   ln -s functions/mass_storage.usb0 configs/c.1
   echo 1 > functions/mass_storage.usb0/lun.0/removable
   echo 0 > functions/mass_storage.usb0/lun.0/cdrom
   echo 0 > functions/mass_storage.usb0/lun.0/ro
   echo 0 > functions/mass_storage.usb0/lun.0/nofua
   echo /tmp/usb >functions/mass_storage.usb0/lun.0/file
   echo "1e6a0000.usb-vhub:p1" >UDC
fi
