FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://8112.cfg \
    file://0001-Add-device-tree-for-AST2600A1-BMC-on-CN8112.patch \
    file://0002-Skip-NCSI-GLS-command.patch \
    file://0003-CN8112-Update-u-boot-env-for-eMMC.patch \
    file://0007-Add-the-code-for-converting-the-eMMC-into-pSLC.patch \
    file://0008-Add-support-to-read-MAC-from-BMC-eeprom.patch \
    "

SRC_URI_append_8112emmc = " \
    file://u-boot-env-ast2600.txt \
    "
