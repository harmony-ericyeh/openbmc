FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += " \
    file://8112.cfg \
    file://0001-CN8112-Initial-device-tree-for-AST2600A1-based-BMC.patch \
    file://0002-Skip-NCSI-GLS-command.patch \
    file://0003-CN8112-Initial-support-of-BIOS-update.patch \
    file://0004-Enable-BMC-watchdog.patch \
    file://0005-Enable-USB-vhub.patch \
    file://0006-Alternate-Bank.patch \
    file://0007-Enable-aspeed-jtag.patch \
    file://0008-rcu-Fix-stall-warning-deadlock-due-to-non-release-of.patch \
"
