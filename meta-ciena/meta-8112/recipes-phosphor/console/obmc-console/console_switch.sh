#!/bin/bash
#
# console_switch.sh
#
# On 8112, one register I2C_DIAG_UART_CONSOLE_SWITCH is provided on I2C
# address space which can be accessed by the BMC. If the Bit 2 of this
# register is set it connects the CPU console to BMC UART2 which is to
# be used for SOL.
#
# UART Console Switch (I2C_DIAG_UART_CONSOLE_SWITCH):
#   ---  -------------------------
#   Bit  Field
#   ---  -------------------------
#    2   BMC_LOCAL_REMOTE_CONSOLE
#    1   BMC_CONSOLE
#    0   CPU_CONSOLE
#   ---  -------------------------

# uart console uart switch value
CPU_CONSOLE=0x01
BMC_CONSOLE=0x02
BMC_LOCAL_REMOTE_CONSOLE=0x04

# i2c
BUS=5
DEV=0x20
DATA=0xC
I2CSET="i2cset -y $BUS $DEV $DATA"
I2CGET="i2cget -y $BUS $DEV $DATA"

usage () {
	echo "Usage: $(basename $0) [ cpu | bmc | sol ]"
    echo "  cpu : CPU console "
    echo "  bmc : BMC console "
    echo "  sol : BMC local remote console"
}

console_setting () {
    _sol="off"
    [ $($I2CGET) = $BMC_LOCAL_REMOTE_CONSOLE ] && _sol="on"

    usage
    echo "SOL Setting : $_sol"
}

console_switch () {
    _con=$1
    echo "Switch to $_con console"
    case "$_con" in
    "cpu")
        $I2CSET $CPU_CONSOLE
        ;;
    "bmc")
        $I2CSET $BMC_CONSOLE
        ;;
    "sol")
        $I2CSET $CPU_CONSOLE
        $I2CSET $BMC_LOCAL_REMOTE_CONSOLE
        ;;
    *) echo "Error: Invalid option."
        usage
        exit 1
        ;;
    esac
}

if [ $# -eq 0 ]; then
    console_setting
    exit 0
fi

type="$1"
console_switch "$type"
exit 0
