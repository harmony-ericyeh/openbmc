FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
RDEPENDS_${PN} += "bash"
OBMC_CONSOLE_HOST_TTY = "ttyS1"

SRC_URI_remove = "file://${BPN}.conf"
SRC_URI += "\
    file://server.ttyS1.conf \
    file://console_switch.sh \
    "

do_install_append() {
    # Remove upstream-provided configuration
    rm -rf ${D}${sysconfdir}/${BPN}

    # Install the server configuration
    install -m 0755 -d ${D}${sysconfdir}/${BPN}
    install -m 0644 ${WORKDIR}/*.conf ${D}${sysconfdir}/${BPN}/

    install -d ${D}/usr/sbin
    install -m 0755 ${WORKDIR}/console_switch.sh ${D}/${sbindir}/console_switch.sh
}
