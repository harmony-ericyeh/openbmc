OBMC_IMAGE_EXTRA_INSTALL_append_8112emmc = " ipmitool"
OBMC_IMAGE_EXTRA_INSTALL_append_8112emmc = " sudo"

INHERIT += "extrausers"
inherit extrausers

EXTRA_USERS_PARAMS_pn-obmc-phosphor-image = ""
# Change the root password to ciena123
EXTRA_USERS_PARAMS_pn-obmc-phosphor-image += "usermod -p '\$1\$UGMqyqdG\$VEo1fs8T2uTvVHKP/u1Rp1' root;"
# add diags user - uid/gid=0 like root also ciena123 passrd
EXTRA_USERS_PARAMS_pn-obmc-phosphor-image += "useradd -g 0 -m -o -u 0 -p '\$1\$UGMqyqdG\$VEo1fs8T2uTvVHKP/u1Rp1' -d /home/root -G sudo diag;"
#add soluser/solpasswd
EXTRA_USERS_PARAMS_pn-obmc-phosphor-image += "useradd -p '\$1\$03PqHspi\$g5limfXXv4A22M87aIPaF/' -G sudo soluser;"

