From 9e67b7b3d6dc8cc27c1a34f3d0013b5a907c4443 Mon Sep 17 00:00:00 2001
From: Shaokong Kao <skao@caxv-skao-2.ciena.com>
Date: Mon, 28 Jun 2021 17:06:32 -0400
Subject: [PATCH] Add ipmid "raw" "application (6)" commands for Ciena

---
 Makefile.am                 |   1 +
 apphandler.cpp              |  10 ++
 cienahandler.cpp            | 187 ++++++++++++++++++++++++++++++++++++
 cienahandler.hpp            |  34 +++++++
 include/ipmid/api-types.hpp |   1 +
 5 files changed, 233 insertions(+)
 create mode 100644 cienahandler.cpp
 create mode 100644 cienahandler.hpp

diff --git a/Makefile.am b/Makefile.am
index 82fdd5d..053253c 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -112,6 +112,7 @@ libipmi20_la_SOURCES = \
 	read_fru_data.cpp \
 	sensordatahandler.cpp \
 	user_channel/channelcommands.cpp \
+	cienahandler.cpp \
 	$(libipmi20_la_TRANSPORTOEM) \
 	$(libipmi20_BUILT_LIST)
 
diff --git a/apphandler.cpp b/apphandler.cpp
index 90818a9..0d56fdd 100644
--- a/apphandler.cpp
+++ b/apphandler.cpp
@@ -38,6 +38,7 @@
 #include <xyz/openbmc_project/Software/Activation/server.hpp>
 #include <xyz/openbmc_project/Software/Version/server.hpp>
 #include <xyz/openbmc_project/State/BMC/server.hpp>
+#include <cienahandler.hpp>
 
 extern sd_bus* bus;
 
@@ -642,6 +643,9 @@ ipmi::RspType<uint8_t,  // Device ID
                 devId.prodId = data.value("prod_id", 0);
                 devId.aux = data.value("aux", 0);
 
+                // Ciena board id
+                devId.id = cienaGetBoardId(devId.prodId);
+
                 // Set the availablitity of the BMC.
                 defaultActivationSetting = data.value("availability", true);
 
@@ -1705,6 +1709,12 @@ void register_netfn_app_functions()
                           ipmi::app::cmdGetSelfTestResults,
                           ipmi::Privilege::User, ipmiAppGetSelfTestResults);
 
+    // <Handle Ciena Commands>
+    ipmi::registerHandler(ipmi::prioOpenBmcBase, ipmi::netFnApp,
+                          ipmi::app::cmdCienaType,
+                          ipmi::Privilege::User, ipmiAppHandleCienaCmds);
+    cienacmd_log_init();
+
     // <Get Device GUID>
     ipmi::registerHandler(ipmi::prioOpenBmcBase, ipmi::netFnApp,
                           ipmi::app::cmdGetDeviceGuid, ipmi::Privilege::User,
diff --git a/cienahandler.cpp b/cienahandler.cpp
new file mode 100644
index 0000000..dc50a3f
--- /dev/null
+++ b/cienahandler.cpp
@@ -0,0 +1,187 @@
+#include <signal.h>
+#include <stdlib.h>
+#include <ctype.h>
+
+#include <cienahandler.hpp>
+
+static const char *ciena_log_file = "/var/log/ciena_ipmi_log";
+static const char *ciena_log_file_old = "/var/log/ciena_ipmi_log.old";
+static const char *ciena_obmc_hostipmi = "/usr/sbin/obmc_hostipmi.sh";
+#define CIENA_LOG_SIZE  (1024 * 1024)
+
+void cienacmd_log_init()
+{
+    FILE *fp;
+    fp = fopen(ciena_log_file, "w");
+    if (!fp)
+        return;
+    fprintf(fp, "Ciena IPMI Log Start\n");
+    fclose(fp);
+}
+
+void cienacmd_log_rollover()
+{
+    struct stat sbuf;
+    if (stat(ciena_log_file, &sbuf) != 0)
+        return;
+    if (sbuf.st_size > CIENA_LOG_SIZE) {
+        rename(ciena_log_file, ciena_log_file_old);
+        cienacmd_log_init();
+    }
+}
+
+void cienacmd_log(char const* buf)
+{
+    FILE *fp;
+    fp = fopen(ciena_log_file, "a");
+    if (!fp)
+        return;
+    fprintf(fp, "%s\n", buf);
+    fclose(fp);
+
+    cienacmd_log_rollover();
+}
+
+static int get_gpioset_pid(int gpio)
+{
+    FILE *fp;
+    char cmdbuf[128];
+    char pidstr[128], *p, *ptr;
+    unsigned int pid = 0;
+
+    sprintf(cmdbuf, "ps | grep gpioset | grep \"%d=\" 2>&1", gpio);
+    fp = popen(cmdbuf, "r");
+    if (fp == NULL) {
+        return (-1);
+    }
+
+    /* Read the output a line at a time - output it. */
+    pidstr[0] = '\0';
+    while (fgets(pidstr, sizeof (pidstr), fp)) {
+        if (strstr(pidstr, "sh -c ps"))
+            continue;
+        cienacmd_log(pidstr);
+        p = pidstr;
+        while(isspace(*p)) p++;
+        ptr = strchr(p, ' ');
+        if (ptr) {
+            *ptr = '\0';
+            pid = atoi(p);
+        }
+    }
+
+    /* close */
+    pclose(fp);
+    return (pid);
+}
+
+static int ipmiAppCienaCodeUpdate(uint8_t arg1, uint8_t arg2)
+{
+    int ret = 0;
+    char cmdbuf[128];
+    int status;
+
+    sprintf(cmdbuf, "%s %d %d", ciena_obmc_hostipmi, arg1, arg2);
+    status = system(cmdbuf);
+    ret = WEXITSTATUS(status);
+    sprintf(cmdbuf, "%s %d %d return %d", ciena_obmc_hostipmi, arg1, arg2, ret);
+    cienacmd_log(cmdbuf);
+
+    return (ret);
+}
+
+static int ipmiAppCienaBiosSel(uint8_t bios_intf, uint8_t bios_cs)
+{
+    int ret, pid;
+    char cmdbuf[128];
+
+    if (bios_intf > 1 || bios_cs > 1)
+        return (-1);
+
+    pid = get_gpioset_pid(149);
+    if (pid) {
+        sprintf(cmdbuf, "kill pid %d", pid);
+        cienacmd_log(cmdbuf);
+        kill(pid, SIGKILL);
+    }
+    pid = get_gpioset_pid(150);
+    if (pid) {
+        sprintf(cmdbuf, "kill pid %d", pid);
+        cienacmd_log(cmdbuf);
+        kill(pid, SIGKILL);
+    }
+
+    if (!bios_intf && !bios_cs) {
+        // No need to hold gpioset
+        sprintf(cmdbuf, "gpioset 0 149=0 150=0");
+    } else {
+        // Need to hold gpioset
+        sprintf(cmdbuf, "gpioset -b --mode=signal 0 149=%d 150=%d", bios_intf, bios_cs);
+    }
+    ret = system(cmdbuf);
+    sleep(1);
+
+    return (ret);
+}
+
+auto ipmiAppHandleCienaCmds(uint8_t cmd, uint8_t arg1, uint8_t arg2) -> ipmi::RspType<uint8_t, uint8_t>
+{
+    char cmdbuf[128];
+    int ret;
+
+    sprintf(cmdbuf, "cmd %d, arg1 %d, arg2 %d", cmd, arg1, arg2);
+    cienacmd_log(cmdbuf);
+
+    switch (cmd) {
+    case CIENA_CMD_BIOS_SEL:
+        ret = ipmiAppCienaBiosSel(arg1, arg2);
+        break;
+    case CIENA_CMD_BMC_UPGD:
+        ret = ipmiAppCienaCodeUpdate(arg1, arg2);
+        break;
+    default:
+        ret = -1;
+        break;
+    }
+    return ipmi::responseSuccess(ret, 0x12);
+}
+
+uint8_t cienaGetBoardId(uint16_t prod)
+{
+    FILE *fp;
+    char cmdbuf[128];
+    char buf[128];
+    int id0, id1;
+    uint8_t bid;
+
+    switch (prod) {
+        case CIENA_PRODID_8112:
+            bid = CIENA_BRDTYPE_8112;
+            break;
+        case CIENA_PRODID_8190:
+            bid = CIENA_BRDTYPE_8190;
+            break;
+        default:
+            return (0);
+    }
+
+    sprintf(cmdbuf, "gpioget 0 96 97");
+    fp = popen(cmdbuf, "r");
+    if (fp == NULL) {
+        return (0);
+    }
+    bzero(buf, 128);
+    if (fgets(buf, sizeof (buf), fp) == NULL) {
+        pclose(fp);
+        return (0);
+    }
+    sscanf(buf, "%d %d", &id0, &id1);
+    pclose(fp);
+
+    bid |= id0 | (id1 << 1);
+    sprintf(buf, "cienaGetBoardId(prod=%d), bid0=%d, bid1=%d, return bid %d",
+        prod, id0, id1, bid);
+    cienacmd_log(buf);
+
+    return (bid);
+}
diff --git a/cienahandler.hpp b/cienahandler.hpp
new file mode 100644
index 0000000..70ba685
--- /dev/null
+++ b/cienahandler.hpp
@@ -0,0 +1,34 @@
+#pragma once
+
+#include <ipmid/api.hpp>
+
+/* Ciena "raw" Application commands */
+#define CIENA_CMD_BIOS_SEL	0
+#define CIENA_CMD_BMC_UPGD	1
+
+/**@brief The Ciena ipmi command.
+ *
+ * @param[in] cmd (defined above)
+ * @param[in] arg1 (cmd specific)
+ * @param[in] arg2 (cmd specific)
+ *
+ * @return
+ * - status
+ **/
+auto ipmiAppHandleCienaCmds(uint8_t cmd, uint8_t arg1, uint8_t arg2) -> ipmi::RspType<uint8_t, uint8_t>;
+
+void cienacmd_log_init();
+
+/*
+ * Variants of AST BMC Based x86 boards
+ * board_type[6:2] on the CPU card BOM
+ */
+#define CIENA_BRDTYPE_8112  0x04
+#define CIENA_BRDTYPE_TBD   0x08
+#define CIENA_BRDTYPE_8190  0x0C
+
+/* Ciena product id */
+#define CIENA_PRODID_8112   8112
+#define CIENA_PRODID_8190   8190
+
+uint8_t cienaGetBoardId(uint16_t prod);
diff --git a/include/ipmid/api-types.hpp b/include/ipmid/api-types.hpp
index 0782a9a..4a44e77 100644
--- a/include/ipmid/api-types.hpp
+++ b/include/ipmid/api-types.hpp
@@ -155,6 +155,7 @@ constexpr Cmd cmdSetCommandSubFnEnables = 0x62;
 constexpr Cmd cmdGetCommandSubFnEnables = 0x63;
 constexpr Cmd cmdGetOemNetFnIanaSupport = 0x64;
 // 0x65-0xff unassigned
+constexpr Cmd cmdCienaType = 0x90;
 } // namespace app
 
 namespace chassis
-- 
2.17.1

