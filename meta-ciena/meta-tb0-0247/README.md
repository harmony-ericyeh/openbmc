This layer allows you to build OpenBMC for 8199
without having to manually configure bblayers.conf and local.conf.

The AST2600 is an ARM, service management SOC made by ASPEED. More information
about the AST2600 can be found
[here](http://aspeedtech.com/server_ast2600/).

To Build:

TEMPLATECONF=meta-ciena/meta-8199/conf/
. openbmc-env
bitbake obmc-phosphor-image

